# Forgejo Template

## Introduction

This is a pre-configured docker container that I use in data science and research. It is mostly the same but it renders math, previews jupyter and I was trying to get a better search with Bleve.

## Features

This template includes:

- Docker files for
  - Katex
    - Template for loading the Katex from the Docker container at `./forgejo_data.template/gitea/templates/custom/footer.tmpl`
  - Adapting Bleve indexing
    - I'm not even sure this works in hindsight
      - It may also break syntax highlighting
- configuration
    - Enabled Bleve indexing
    - Renders Jupyter, `.rst`, `.odt` etc.

## Usage

1. `[OPTIONAL]` Forgejo Container

    In this optional step, you can build the Forgejo container. Please note that doing this might break syntax highlighting (`TODO` confirm this).

    The Dockerfile for the patched indexer is located in the `./Dockerfiles/Patched Indexer/`.

    ```bash
    cd ./Dockerfiles/Patched\ Indexer/
    docker build -t forgejo-patched-indexer .
    ```

    1. Update the desired image name in the `./docker-compose.yml` file.

1. KaTeX container

    Build the Docker container for KaTeX using this command:

    ```bash
    cd ./Dockerfiles/KaTeX/
    docker build -t katex .
    ```
    Duplicate the `footer.tmpl` from the directory `./forgejo_data.template/gitea/templates/custom/`.

1. Post-Start Actions

    * Render other Formats

        After starting the container, bring it down and update the configuration files (see `./forgejo_data.template/gitea/conf/app.ini`) to enable Jupyter preview.

    * Check the GITEA_CUSOM directory

        1. Env Var

            It can be helpful later to know what `GITEA_CUSTOM` is. The last I checked, in Docker, it is set to `/data/gitea`, which is mapped to `./forgejo_data.template` in the `./docker-compose.yml`.

            ```bash
            docker compose exec -it server /bin/bash
            echo $GITEA_CUSTOM
            ```
        1. Env Var
            It may also be documented in the site-administration/config page at <http://localhost:3000/admin/config>.
